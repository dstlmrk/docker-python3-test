FROM python:3-alpine
ENV TZ Europe/Prague
RUN apk --update add mysql-client mariadb-dev gcc g++ linux-headers
ADD requirements.txt /tmp/requirements.txt
RUN pip install --no-cache-dir -r /tmp/requirements.txt
ADD ./bin/entrypoint.sh /
ENTRYPOINT ["/entrypoint.sh"]
